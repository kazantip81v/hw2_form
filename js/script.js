window.onload = function() {

	let user = {};
	let colection, 
		length;

		colection = document.querySelectorAll('input');
		length = colection.length;

	let validate = function() {
		let count = length;

		colection.forEach(function(item, i, arr) {
			if(colection[i].value === '') {
				colection[i].parentNode.classList.add('field-empty');
				setTimeout(function() {
					colection[i].parentNode.classList.remove('field-empty');
				}, 1700);
				count--;
			};
		});
		return count;
	};

	let arrVal = function () {
		let count = validate();
		let arrValue = [];

		if (count === length) {
			colection.forEach(function(item, i, arr) {
  				arrValue.push(colection[i].value);
			});
		};
		return arrValue;
	};

	let clearValue = function() {
		colection.forEach(function(item, i, arr) {
  			colection[i].value = '';
		});
	};

	function addLocalStorage () {

		let arrValue = arrVal();
		
		if (arrValue.length !== 0 ) {

			colection.forEach(function(item, i, arr) {
			user[colection[i].name] = arrValue[i];
		});

			let userObj = JSON.stringify(user);
			localStorage.setItem('User', userObj);
			clearValue();
		};
	};

	document.querySelector('.js-addLocStor').addEventListener("click", addLocalStorage);
};